** Derail Valley Overhauled Save Editor**  
This is a save game editor for Derail Valley Overhauled.  
With this program, you can edit the values of your wallet, licenses, locomotives and wagons stats.  
Furthermore you can view and edit available jobs.  
  
*07/06/2020: Updated for Build #81*  
  
System requirements:  
Windows PC with .net 4.7.2  
  
Please use this editor with care and **ALWAYS** make a backup of your savaegame.  
## I am not responsible if you destroy your save file  
  
Published under MIT license