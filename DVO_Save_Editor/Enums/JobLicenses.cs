﻿using System;

namespace DVO_Save_Editor.Enums
{
    [Flags]
    public enum JobLicenses
    {
        Basic = 0,
        Hazmat1 = 1,
        Hazmat2 = 2,
        Hazmat3 = 4,
        Military1 = 8,
        Military2 = 16,
        Military3 = 32,
        FreightHaul = 512,
        Shunting = 1024,
        LogisticHaul = 2048,
        TrainLength1 = 16384,
        TrainLength2 = 32768
    }

    public enum TaskState
    {
        Done,
        InProgress,
        Failed,
    }

    public enum TaskType
    {
        Transport,
        Warehouse,
        Sequential,
        Parallel,
    }
}