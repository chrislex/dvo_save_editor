﻿namespace DVO_Save_Editor.Enums
{
    public enum CarTypes
    {
        NotSet = 0,
        LocoShunter = 10, // 0x0000000A
        LocoSteamHeavy = 20, // 0x00000014
        Tender = 21, // 0x00000015
        LocoSteamHeavyBlue = 22, // 0x00000016
        TenderBlue = 23, // 0x00000017
        LocoRailbus = 30, // 0x0000001E
        LocoDiesel = 40, // 0x00000028
        FlatbedEmpty = 200, // 0x000000C8
        FlatbedStakes = 201, // 0x000000C9
        FlatbedMilitary = 202, // 0x000000CA
        AutorackRed = 250, // 0x000000FA
        AutorackBlue = 251, // 0x000000FB
        AutorackGreen = 252, // 0x000000FC
        AutorackYellow = 253, // 0x000000FD
        TankOrange = 300, // 0x0000012C
        TankWhite = 301, // 0x0000012D
        TankYellow = 302, // 0x0000012E
        TankBlue = 303, // 0x0000012F
        TankChrome = 304, // 0x00000130
        TankBlack = 305, // 0x00000131
        BoxcarBrown = 400, // 0x00000190
        BoxcarGreen = 401, // 0x00000191
        BoxcarPink = 402, // 0x00000192
        BoxcarRed = 403, // 0x00000193
        BoxcarMilitary = 404, // 0x00000194
        RefrigeratorWhite = 450, // 0x000001C2
        HopperBrown = 500, // 0x000001F4
        HopperTeal = 501, // 0x000001F5
        HopperYellow = 502, // 0x000001F6
        PassengerRed = 600, // 0x00000258
        PassengerGreen = 601, // 0x00000259
        PassengerBlue = 602, // 0x0000025A
        HandCar = 700, // 0x000002BC
        NuclearFlask = 800, // 0x00000320
    }
}
