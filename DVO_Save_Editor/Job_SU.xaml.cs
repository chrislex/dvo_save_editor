﻿namespace DVO_Save_Editor
{
    /// <summary>
    /// Interaction logic for Job_SU.xaml
    /// </summary>
    public partial class Job_SU
    {
        public Job_SU(UnloadJobDefinitionData job)
        {
            InitializeComponent();
            DataContext = job;
        }
    }
}
