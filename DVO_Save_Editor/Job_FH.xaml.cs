﻿using System.Windows.Media;

namespace DVO_Save_Editor
{
    /// <summary>
    /// Interaction logic for Job_FH.xaml
    /// </summary>
    public partial class Job_FH
    {
        public Job_FH(JobDefinitionDataBase job)
        {
            InitializeComponent();
            if (job is TransportJobDefinitionData)
            {
                Headline.Text = "FREIGHT HAUL";
                Headline.Background = new SolidColorBrush(Color.FromRgb(75,151,87));
            }
            else
            {
                Headline.Text = "LOGISTICAL HAUL";
                Headline.Background = new SolidColorBrush(Color.FromRgb(161,148,93));
            }
            DataContext = job;
        }
    }
}
