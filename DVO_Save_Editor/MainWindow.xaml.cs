﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DVO_Save_Editor.DataModels;
using DVO_Save_Editor.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DVO_Save_Editor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            LoadSave();
        }

        private void LoadSave()
        {
            DataContext = null;
            Globals.Instance.dataModel = new MainViewModel();
            Globals.Instance.SavePath = Methods.GetDVInstallLocation();
            if (Globals.Instance.SavePath == null)
            {
                var dlg = new FolderBrowserForWPF.Dialog {Title = "Select the Derail Valley folder"};
                dlg.ShowDialog();
                Globals.Instance.SavePath = Methods.CheckPath(dlg.FileName);
                if (Globals.Instance.SavePath == null)
                {
                    MessageBox.Show("I cannot find the Derail Valley save file.\nThe Application will quit now",
                        "Save file not found", MessageBoxButton.OK);
                    Application.Current.Shutdown();
                    return;
                }
            }

            var saveFile = File.ReadAllText(Globals.Instance.SavePath);
            var saveString = Methods.DecryptString(saveFile);
            try
            {
                Globals.Instance.jdoc = JObject.Parse(saveString);
            }
            catch (Exception)
            {
                MessageBox.Show("Cannot parse the save file.\nThe Application will quit now.", "Error",
                    MessageBoxButton.OK);
                Application.Current.Shutdown();
                return;
            }

            // Build #81:
            // I had two different Cars and Job sections in my savegame.
            // Get all the hash values and the parse the cars and jobs and merge them into one hash value object
            var hash_lst = (from tok in Globals.Instance.jdoc.Root
                where tok.Path.Contains("Cars#")
                select ((JObject) tok.First)?["trackHash"]?.ToString()).ToList();

            Globals.Instance.Hash = hash_lst.Last();
            Globals.Instance.dataModel.player = Globals.Instance.jdoc.Root.ToObject<PlayerModel>();


            for (var i = hash_lst.Count - 1; i >= 0; i--)
            {
                // Parse the jobs
                if (Globals.Instance.jdoc["Jobs#" + hash_lst[i]] != null)
                {
                    var jobs_tmp = Globals.Instance.jdoc["Jobs#" + hash_lst[i]]?.ToString()
                        .Replace("Assembly-CSharp", "DVO_Save_Editor")
                        .Replace("DV.Logic.Job.", "DVO_Save_Editor.Enums.");
                    if (jobs_tmp != null)
                    {
                        if (i == hash_lst.Count - 1)
                        {
                            Globals.Instance.dataModel.jobs =
                                JsonConvert.DeserializeObject<JobsSaveGameData>(jobs_tmp,
                                    Globals.Instance.dataModel.jsonSerializerSettings);
                        }
                        else
                        {
                            if (Globals.Instance.dataModel.jobs != null)
                            {
                                var tmp_job = JsonConvert.DeserializeObject<JobsSaveGameData>(jobs_tmp,
                                    Globals.Instance.dataModel.jsonSerializerSettings);
                                if (tmp_job != null)
                                {
                                    var tmp_orig = Globals.Instance.dataModel.jobs.jobChains.ToList();
                                    tmp_orig.AddRange(tmp_job.jobChains.ToList());
                                    Globals.Instance.dataModel.jobs.jobChains = tmp_orig.ToArray();
                                }
                            }

                            Globals.Instance.jdoc["Jobs#" + hash_lst[i]]?.Parent?.Remove();
                        }
                    }
                }

                // Parse the cars
                if (i == hash_lst.Count - 1) continue;
                //var tmp_cars = jdoc["Cars#" + hash_lst[i]]?["carsData"]?.ToArray();
                ((JArray)Globals.Instance.jdoc["Cars#" + hash_lst.Last()]?["carsData"])?.Add(Globals.Instance.jdoc["Cars#" + hash_lst[i]]?["carsData"]
                    ?.ToArray());
                Globals.Instance.jdoc["Cars#" + hash_lst[i]]?.Parent?.Remove();
            }

            // Parse the cars and sort them
            Globals.Instance.dataModel.cars = Globals.Instance.jdoc["Cars#" + Globals.Instance.Hash]?.ToObject<CarsViewModel>();
            var c1_tmp = Globals.Instance.dataModel.cars?.carsData.Where(j => j.id.Contains("-")).OrderBy(j => j.id)
                .ToList();
            if (c1_tmp != null)
            {
                var c2_tmp = Globals.Instance.dataModel.cars?.carsData.Except(c1_tmp).OrderBy(j => j.id).ToList();
                c2_tmp.InsertRange(0, c1_tmp);
                Globals.Instance.dataModel.cars.carsData = new ObservableCollection<CarsModel>(c2_tmp);
            }

            DataContext = Globals.Instance.dataModel;
        }

        private void MenuItem_Load(object sender, ExecutedRoutedEventArgs e)
        {
            LoadSave();
        }

        private void MenuItem_Save(object sender, ExecutedRoutedEventArgs e)
        {
            scrl.Focus();
            // Write the player data back to json
            Globals.Instance.jdoc["License_DE2"] = Globals.Instance.dataModel.player.License_DE2;
            Globals.Instance.jdoc["License_DE6"] = Globals.Instance.dataModel.player.License_DE6;
            Globals.Instance.jdoc["License_SH282"] = Globals.Instance.dataModel.player.License_SH282;
            Globals.Instance.jdoc["License_ConcurrentJobs1"] = Globals.Instance.dataModel.player.License_ConcurrentJobs1;
            Globals.Instance.jdoc["License_ConcurrentJobs2"] = Globals.Instance.dataModel.player.License_ConcurrentJobs2;
            Globals.Instance.jdoc["License_ManualService"] = Globals.Instance.dataModel.player.License_ManualService;
            Globals.Instance.jdoc["Job_Licenses"] = (int) Globals.Instance.dataModel.player.Job_Licenses;
            Globals.Instance.jdoc["Player_money"] = Globals.Instance.dataModel.player.Player_money;

            // Write the cars data back to json
            Globals.Instance.dataModel.cars.writeData(Globals.Instance.jdoc);

            // Prepare the job data and write is back to json
            Globals.Instance.dataModel.jobs.jobChains = Globals.Instance.dataModel.jobs.jobChains2.ToArray();

            var json_job = JsonConvert.SerializeObject(Globals.Instance.dataModel.jobs,
                Globals.Instance.dataModel.jsonSerializerSettings);
            if (json_job != string.Empty)
            {
                var replace = json_job.Replace("DVO_Save_Editor.Enums.", "DV.Logic.Job.")
                    .Replace("DVO_Save_Editor", "Assembly-CSharp").Replace("System.Private.CoreLib", "mscorlib");
                Globals.Instance.jdoc["Jobs#" + Globals.Instance.Hash] = replace;
            }

            var newjson = Globals.Instance.jdoc.ToString(Formatting.None);
            File.WriteAllText(Globals.Instance.SavePath, Methods.EncryptString(newjson));
        }

        private void MenuItem_Exit(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void ListCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListCars.SelectedIndex == -1) return;
            Globals.Instance.dataModel.selectedCar = Globals.Instance.dataModel.cars.carsData[ListCars.SelectedIndex];
        }

        private void ListJobs_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListJobs.SelectedIndex == -1) return;
            Globals.Instance.dataModel.selectedJob = Globals.Instance.dataModel.jobs.jobChains2[ListJobs.SelectedIndex];
            ListChain.SelectedIndex = 0;
        }

        private void ListChain_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ls = (ListView) sender;
            switch (ls.SelectedItem)
            {
                case LoadJobDefinitionData tmp_sl:
                    Frame_Job.Content = new Job_SL(tmp_sl);
                    break;
                case UnloadJobDefinitionData tmp_su:
                    Frame_Job.Content = new Job_SU(tmp_su);
                    break;
                case JobDefinitionDataBase tmp_h:
                    Frame_Job.Content = new Job_FH(tmp_h);
                    break;
            }
        }
    }

    public class RelayCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public RelayCommand(Predicate<object> canExecute, Action<object> execute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}