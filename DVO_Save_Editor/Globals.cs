﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DVO_Save_Editor.ViewModels;
using Newtonsoft.Json.Linq;

namespace DVO_Save_Editor
{
    public sealed class Globals : INotifyPropertyChanged
    {
        private static readonly Lazy<Globals> lazy = new Lazy<Globals>(()=>new Globals());
        private MainViewModel _dataModel;

        public static Globals Instance => lazy.Value;

        private Globals()
        {
            dataModel = new MainViewModel();
        }

        public MainViewModel dataModel
        {
            get => _dataModel;
            set
            {
                if (Equals(value, _dataModel)) return;
                _dataModel = value;
                OnPropertyChanged();
            }
        }
        public string Hash { get; set; }
        public string SavePath { get; set; }
        public JObject jdoc { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}