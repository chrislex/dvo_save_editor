﻿namespace DVO_Save_Editor
{
    /// <summary>
    /// Interaction logic for Job_SL.xaml
    /// </summary>
    public partial class Job_SL
    {
        public Job_SL(LoadJobDefinitionData job)
        {
            InitializeComponent();
            DataContext = job;
        }
    }
}
