﻿namespace DVO_Save_Editor.DataModels
{
    public class LocoState_DmgModel
    {
        public decimal bodyHP { get; set; }
        public decimal wheelsHP { get; set; }
        public decimal? engineHP { get; set; }
        public bool? windowsBroken { get; set; }
    }
}