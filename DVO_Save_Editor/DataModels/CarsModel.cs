﻿using DVO_Save_Editor.Enums;
using DVO_Save_Editor.ViewModels;

namespace DVO_Save_Editor.DataModels
{
    public class CarsModel
    {
        public string id { get; set; }
        public string carGuid { get; set; }
        public CarTypes type { get; set; }
        public CargoType loadedCargo { get; set; }
        public LocoStateViewModel locoState { get; set; }
        public CarStateViewModel carState { get; set; }
    }
}