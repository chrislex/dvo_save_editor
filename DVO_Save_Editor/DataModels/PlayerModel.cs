﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using DVO_Save_Editor.Enums;

namespace DVO_Save_Editor.DataModels
{
    public class PlayerModel : INotifyPropertyChanged
    {
        private bool _licenseConcurrentJobs1;
        private bool _licenseConcurrentJobs2;
        private bool _licenseDe2;
        private bool _licenseDe6;
        private bool _licenseSh282;
        private decimal _playerMoney;
        private bool _licenseTrainLength1;
        private bool _licenseTrainLength2;

        private JobLicenses _jobLicenses;
        private bool _hazmat1;
        private bool _hazmat2;
        private bool _hazmat3;
        private bool _military1;
        private bool _military2;
        private bool _military3;
        private bool _freightHaul;
        private bool _logisticHaul;
        private bool _shunting;
        private bool _manualService;

        public bool License_DE2
        {
            get => _licenseDe2;
            set
            {
                if (value == _licenseDe2) return;
                _licenseDe2 = value;
                OnPropertyChanged();
            }
        }

        public bool License_DE6
        {
            get => _licenseDe6;
            set
            {
                if (value == _licenseDe6) return;
                _licenseDe6 = value;
                OnPropertyChanged();
            }
        }

        public bool License_SH282
        {
            get => _licenseSh282;
            set
            {
                if (value == _licenseSh282) return;
                _licenseSh282 = value;
                OnPropertyChanged();
            }
        }

        public bool License_ConcurrentJobs1
        {
            get => _licenseConcurrentJobs1;
            set
            {
                if (value == _licenseConcurrentJobs1) return;
                _licenseConcurrentJobs1 = value;
                OnPropertyChanged();
            }
        }

        public bool License_ConcurrentJobs2
        {
            get => _licenseConcurrentJobs2;
            set
            {
                if (value == _licenseConcurrentJobs2) return;
                _licenseConcurrentJobs2 = value;
                OnPropertyChanged();
            }
        }

        public bool TrainLength1
        {
            get => _licenseTrainLength1;
            set
            {
                if (value == _licenseTrainLength1) return;
                _licenseTrainLength1 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.TrainLength1);
            }
        }

        public bool TrainLength2
        {
            get => _licenseTrainLength2;
            set
            {
                if (value == _licenseTrainLength2) return;
                _licenseTrainLength2 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.TrainLength2);
            }
        }

        public bool Hazmat1
        {
            get => _hazmat1;
            set
            {
                if (value == _hazmat1) return;
                _hazmat1 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Hazmat1);
            }
        }

        public bool Hazmat2
        {
            get => _hazmat2;
            set
            {
                if (value == _hazmat2) return;
                _hazmat2 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Hazmat2);
            }
        }

        public bool Hazmat3
        {
            get => _hazmat3;
            set
            {
                if (value == _hazmat3) return;
                _hazmat3 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Hazmat3);
            }
        }

        public bool Military1
        {
            get => _military1;
            set
            {
                if (value == _military1) return;
                _military1 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Military1);
            }
        }

        public bool Military2
        {
            get => _military2;
            set
            {
                if (value == _military2) return;
                _military2 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Military2);
            }
        }

        public bool Military3
        {
            get => _military3;
            set
            {
                if (value == _military3) return;
                _military3 = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Military3);
            }
        }

        public bool FreightHaul
        {
            get => _freightHaul;
            set
            {
                if (value == _freightHaul) return;
                _freightHaul = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.FreightHaul);
            }
        }

        public bool LogisticHaul
        {
            get => _logisticHaul;
            set
            {
                if (value == _logisticHaul) return;
                _logisticHaul = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.LogisticHaul);
            }
        }

        public bool Shunting
        {
            get => _shunting;
            set
            {
                if (value == _shunting) return;
                _shunting = value;
                OnPropertyChanged();
                Enumchanger(value, JobLicenses.Shunting);
                
            }
        }

        public bool License_ManualService
        {
            get => _manualService;
            set
            {
                if (value == _manualService) return;
                _manualService = value;
                OnPropertyChanged();
            }
        }

        public JobLicenses Job_Licenses
        {
            get => _jobLicenses;
            set
            {
                _jobLicenses = value;
                TrainLength1 = value.HasFlag(JobLicenses.TrainLength1);
                TrainLength2 = value.HasFlag(JobLicenses.TrainLength2);
                Hazmat1 = value.HasFlag(JobLicenses.Hazmat1);
                Hazmat2 = value.HasFlag(JobLicenses.Hazmat2);
                Hazmat3 = value.HasFlag(JobLicenses.Hazmat3);
                Military1 = value.HasFlag(JobLicenses.Military1);
                Military2 = value.HasFlag(JobLicenses.Military2);
                Military3 = value.HasFlag(JobLicenses.Military3);
                FreightHaul = value.HasFlag(JobLicenses.FreightHaul);
                LogisticHaul = value.HasFlag(JobLicenses.LogisticHaul);
                Shunting = value.HasFlag(JobLicenses.Shunting);
            }
        }

       

        public decimal Player_money
        {
            get => _playerMoney;
            set
            {
                if (value == _playerMoney) return;
                _playerMoney = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Enumchanger(bool value, JobLicenses license)
        {
            if (!value && _jobLicenses.HasFlag(license))
            {
                _jobLicenses &= ~license;
            }
            else if (value && !_jobLicenses.HasFlag(license))
            {
                _jobLicenses |= license;
            }
        }
    }
}