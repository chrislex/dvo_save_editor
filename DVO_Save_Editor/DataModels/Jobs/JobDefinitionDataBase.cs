﻿using DVO_Save_Editor.Enums;

public abstract class JobDefinitionDataBase
{
    public float timeLimitForJob { get; set; }
    public float initialWage { get; set; }
    public string stationId { get; set; }
    public string originStationId { get; set; }
    public string destinationStationId { get; set; }
    public int requiredLicenses { get; set; }
}

public class EmptyHaulJobDefinitionData : JobDefinitionDataBase
{
    public string[] transportCarGuids{get;set;}
    public string startTrackId{get;set;}
    public string destinationTrackId{get;set;}
}

public class LoadJobDefinitionData : JobDefinitionDataBase
{
    public CarGuidsPerTrackId[] carGuidsPerStartingTrackId{get;set;}
    public CarGuidsPerCargo[] carGuidsPerLoadCargo{get;set;}
    public string loadMachineId{get;set;}
    public string destinationTrackId{get;set;}
}

namespace MyNamespace
{
    
}
public class TransportJobDefinitionData : JobDefinitionDataBase
{
    public string[] transportCarGuids { get; set; }
    public string startTrackId { get; set; }
    public string destinationTrackId { get; set; }
    public CargoType[] transportedCargoPerCar { get; set; }
    public float[] cargoAmountPerCar { get; set; }
}

public class UnloadJobDefinitionData : JobDefinitionDataBase
{
    public string startingTrackId { get; set; }
    public CarGuidsPerTrackId[] carGuidsPerDestinationTrackId { get; set; }
    public CarGuidsPerCargo[] carGuidsPerUnloadCargo { get; set; }
    public string unloadMachineId { get; set; }
}