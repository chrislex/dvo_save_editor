﻿public class JobChainSaveData
{
    public JobDefinitionDataBase[] jobChainData { get; set; }
    public string[] trainCarGuids { get; set; }
    public bool jobTaken { get; set; }
    public TaskSaveData[] currentJobTaskData { get; set; }
    public string firstJobId { get; set; }
}