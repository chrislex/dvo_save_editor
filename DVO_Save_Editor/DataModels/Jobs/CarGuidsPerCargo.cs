﻿using DVO_Save_Editor.Enums;

public class CarGuidsPerCargo
{
    public CargoType cargo { get; set; }
    public string[] carGuids { get; set; }
    public float totalCargoAmount { get; set; }
}