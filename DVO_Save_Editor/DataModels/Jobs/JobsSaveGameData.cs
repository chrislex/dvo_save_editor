﻿using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;

public class JobsSaveGameData
{
    [JsonIgnore] private JobChainSaveData[] _jobChains;

    public JobChainSaveData[] jobChains
    {
        get => _jobChains;
        set
        {
            _jobChains = value;
            jobChains2 = new ObservableCollection<JobChainSaveData>(jobChains.OrderBy(c=>c.firstJobId));
        }
    }

    [JsonIgnore] public ObservableCollection<JobChainSaveData> jobChains2 { get; set; }
    public float logicTimer { get; set; }
}