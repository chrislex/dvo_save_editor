﻿using DVO_Save_Editor.Enums;

public class TaskSaveData
{
    public TaskState state { get; set; }
    public TaskType type { get; set; }
    public float taskStartTime { get; set; }
    public float taskFinishedTime { get; set; }
}