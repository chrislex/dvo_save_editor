﻿namespace DVO_Save_Editor.DataModels
{
    public class LocoState_SimModel
    {
        public decimal? Fuel { get; set; }
        public decimal? Oil { get; set; }
        public decimal Sand { get; set; }
        public decimal? EngineTemperature { get; set; }
        public decimal? fuelConsumed { get; set; }
        public decimal? TenderWater { get; set; }
        public decimal? TenderCoal { get; set; }
        public decimal? FireOn { get; set; }
        public decimal? Temperature { get; set; }
        public decimal? CoalBox { get; set; }
        public decimal? BoilerWater { get; set; }
        public decimal? BoilerPressure { get; set; }
        public decimal? coalConsumed { get; set; }
    }
}