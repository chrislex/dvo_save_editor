﻿using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using DVO_Save_Editor.DataModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DVO_Save_Editor.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            cars = new CarsViewModel();
        }

        private CarsModel _selectedCar;
        public PlayerModel player { get; set; }
        public CarsViewModel cars { get; set; }
        public JobsSaveGameData jobs { get; set; }

        public CarsModel selectedCar
        {
            get => _selectedCar;
            set
            {
                if (Equals(value, _selectedCar)) return;
                _selectedCar = value;
                OnPropertyChanged();
            }
        }

        public JobChainSaveData selectedJob
        {
            get => _selectedJob;
            set
            {
                if (Equals(value, _selectedJob)) return;
                _selectedJob = value;
                OnPropertyChanged();
            }
        }

        public readonly JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        private JobChainSaveData _selectedJob;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private ICommand _RemoveItem;

        public ICommand RemoveItem =>
            _RemoveItem ?? (_RemoveItem = new RelayCommand(
                p => true, removeObject));

        private void removeObject(object parameter)
        {
            if (parameter is CarsModel car)
            {
                var guid = car.carGuid;
               
                var array = (JArray) Globals.Instance.jdoc["Cars#" + Globals.Instance.Hash]?["carsData"];
                if (array != null)
                {
                    var tmp = array.First(j => j["carGuid"]?.ToString() == guid);
                    foreach (var jobs_lst in jobs.jobChains2)
                    {
                        var job = jobs_lst.trainCarGuids.Where(k => k == guid);
                        if (job.Count() != 0)
                        {
                            MessageBox.Show("This car is used in a job. Cannot delete this car.",
                                "Cannot delete this car.");
                            return;
                        }
                    }
                    Globals.Instance.dataModel.cars.carsData.Remove(car);
                    tmp?.Remove();
                }
            }
        }
    }
}