﻿namespace DVO_Save_Editor.ViewModels
{
    public class CarStateViewModel
    {
        public decimal carDmg { get; set; }
        public decimal cargoDmg { get; set; }

        public DebtModel debt { get; set; }

        public class DebtModel
        {
            public decimal carStartV { get; set; }
            public decimal cargoStartV { get; set; }
            public bool cargoUnloaded { get; set; }
            public decimal cargoEndV { get; set; }
            public int loadedCargo { get; set; }
        }
    }
}