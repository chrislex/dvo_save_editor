﻿using System.Collections.ObjectModel;
using System.Linq;
using DVO_Save_Editor.DataModels;
using DVO_Save_Editor.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DVO_Save_Editor.ViewModels
{
    public class CarsViewModel
    {
        public CarsViewModel()
        {
            carsData = new ObservableCollection<CarsModel>();
        }
        public string trackHash { get; set; }
        public ObservableCollection<CarsModel> carsData { get; set; }

        public void writeData(JObject doc)
        {
            var jcar = doc["Cars#" + trackHash]?["carsData"];
            foreach (var car in carsData)
            {
                var car_tmp = jcar?.Children<JObject>()
                    .FirstOrDefault(c => c.Value<string>("carGuid") == car.carGuid);
                if (car_tmp == null)
                {
                    continue;
                }

                car_tmp["type"] = (int) car.type;
                car_tmp["loadedCargo"] = (int) car.loadedCargo;
                var serializerNull = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                switch (car.type)
                {
                    case CarTypes.LocoDiesel:
                    case CarTypes.LocoRailbus:
                    case CarTypes.LocoShunter:
                        car_tmp.Remove("carState");
                        car.locoState.sim.BoilerPressure = null;
                        car.locoState.sim.BoilerWater = null;
                        car.locoState.sim.CoalBox = null;
                        car.locoState.sim.FireOn = null;
                        car.locoState.sim.Temperature = null;
                        car.locoState.sim.TenderCoal = null;
                        car.locoState.sim.TenderWater = null;
                        car.locoState.sim.coalConsumed = null;
                        car_tmp["locoState"] =
                            JObject.Parse(JsonConvert.SerializeObject(car.locoState, serializerNull));
                        break;
                    case CarTypes.LocoSteamHeavy:
                    case CarTypes.LocoSteamHeavyBlue:
                        car_tmp.Remove("carState");
                        car.locoState.engineOn = null;
                        car.locoState.sim.Oil = null;
                        car.locoState.sim.Fuel = null;
                        car.locoState.sim.fuelConsumed = null;
                        car.locoState.sim.EngineTemperature = null;
                        car.locoState.sim.TenderCoal = null;
                        car.locoState.sim.TenderWater = null;
                        car.locoState.dmg.windowsBroken = null;
                        car.locoState.dmg.engineHP = null;
                        car_tmp["locoState"] =
                            JObject.Parse(JsonConvert.SerializeObject(car.locoState, serializerNull));
                        break;
                    case CarTypes.Tender:
                    case CarTypes.TenderBlue:
                        car.locoState.engineOn = null;
                        car.locoState.sim.Oil = null;
                        car.locoState.sim.Fuel = null;
                        car.locoState.sim.fuelConsumed = null;
                        car.locoState.sim.EngineTemperature = null;
                        car.locoState.sim.BoilerPressure = null;
                        car.locoState.sim.BoilerWater = null;
                        car.locoState.sim.CoalBox = null;
                        car.locoState.sim.FireOn = null;
                        car.locoState.sim.coalConsumed = null;
                        car.locoState.sim.Temperature = null;
                        car.locoState.dmg = null;
                        car_tmp["locoState"] =
                            JObject.Parse(JsonConvert.SerializeObject(car.locoState, serializerNull));
                        car_tmp["carState"] = JObject.Parse(JsonConvert.SerializeObject(car.carState, serializerNull));
                        break;
                    default:
                        car_tmp.Remove("locoState");
                        car_tmp["carState"] = JObject.Parse(JsonConvert.SerializeObject(car.carState, serializerNull));
                        break;
                }
            }
        }
    }
}