﻿using DVO_Save_Editor.DataModels;

namespace DVO_Save_Editor.ViewModels
{
    public class LocoStateViewModel
    {
        public LocoState_SimModel sim { get; set; }
        public LocoState_DmgModel dmg { get; set; }
        public bool? engineOn { get; set; }
        public decimal? visit { get; set; }
    }
}