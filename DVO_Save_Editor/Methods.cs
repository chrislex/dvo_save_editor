﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Win32;
namespace DVO_Save_Editor
{
    public class Methods
    {
        private const string pass = "WeDidntSecureThisVeryWell!!1";

        public static string GetDVInstallLocation()
        {
            const int AppID = 588030;
            using (var regKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32)
                .OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App " + AppID, false))
            {
                if (regKey != null)
                {
                    return CheckPath(regKey.GetValue("InstallLocation").ToString());
                }
            }

            using (var regKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                .OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App " + AppID, false))
            {
                return regKey == null ? string.Empty : CheckPath(regKey.GetValue("InstallLocation").ToString());
            }
        }

        /// <summary>
        /// Checks if Derail Valley savegame path exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string CheckPath(string path)
        {
            try
            {
                var tmp = Path.Combine(path, "DerailValley_Data", "SaveGameData", "savegame");
                return Directory.Exists(path) && File.Exists(tmp) ? tmp : null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string DecryptString(string cipher)
        {
            var bytes1 = Encoding.UTF8.GetBytes("pemgail9uzpgzl88");
            var buffer = Convert.FromBase64String(cipher);
            var bytes2 = new PasswordDeriveBytes(pass, null).GetBytes(32);
            
            var bytes3 = new PasswordDeriveBytes(pass,null);
            var rijndaelManaged = new RijndaelManaged { Mode = CipherMode.CBC };
            var decryptor = rijndaelManaged.CreateDecryptor(bytes2, bytes1);

            using (var memoryStream = new MemoryStream(buffer))
            {
                using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                {
                    var numArray = new byte[buffer.Length];
                    var count = cryptoStream.Read(numArray, 0, numArray.Length);
                    return Encoding.UTF8.GetString(numArray, 0, count);
                }
            }
        }

        public static string EncryptString(string text)
        {
            var bytes1 = Encoding.UTF8.GetBytes("pemgail9uzpgzl88");
            var bytes2 = Encoding.UTF8.GetBytes(text);
            var bytes3 = new PasswordDeriveBytes(pass, null).GetBytes(32);
            var rijndaelManaged = new RijndaelManaged { Mode = CipherMode.CBC };
            var encryptor = rijndaelManaged.CreateEncryptor(bytes3, bytes1);
            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(bytes2, 0, bytes2.Length);
                    cryptoStream.FlushFinalBlock();
                    var array = memoryStream.ToArray();
                    return Convert.ToBase64String(array);
                }
            }
        }
    }
}