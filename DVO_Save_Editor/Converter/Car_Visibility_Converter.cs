﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using DVO_Save_Editor.Enums;

namespace DVO_Save_Editor.Converter
{
    internal class Car_Visibility_Converter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == DependencyProperty.UnsetValue || values == null || parameter == null)
            {
                return Visibility.Collapsed;
            }

            switch ((CarTypes) values)
            {
                case CarTypes.LocoRailbus:
                case CarTypes.LocoDiesel:
                case CarTypes.LocoShunter:
                    return parameter.Equals("Diesel") || parameter.Equals("Loco")
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                case CarTypes.LocoSteamHeavy:
                case CarTypes.LocoSteamHeavyBlue:
                    return parameter.Equals("Steam") || parameter.Equals("Loco")
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                case CarTypes.Tender:
                case CarTypes.TenderBlue:
                    return parameter.Equals("Tender") ? Visibility.Visible : Visibility.Collapsed;
                default:
                    return parameter.Equals("Car") ? Visibility.Visible : Visibility.Collapsed;
            }

            /*return Methods.IsLoco((CarTypes) values) ? parameter.Equals("Loco") ? Visibility.Visible :
                Visibility.Collapsed :
                parameter.Equals("Car") ? Visibility.Visible : Visibility.Collapsed;*/
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}