﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using DVO_Save_Editor.Enums;

namespace DVO_Save_Editor.Converter
{
    internal class Car_Max_Converter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == DependencyProperty.UnsetValue || values == null || parameter == null)
            {
                return null;
            }

            switch ((CarTypes) values)
            {
                case CarTypes.LocoDiesel:
                    if (parameter.Equals("Fuel"))
                    {
                        return 6000m;
                    }

                    if (parameter.Equals("Oil"))
                    {
                        return 500m;
                    }

                    if (parameter.Equals("Sand"))
                    {
                        return 200m;
                    }

                    if (parameter.Equals("Wheels"))
                    {
                        return 2000m;
                    }

                    if (parameter.Equals("Engine"))
                    {
                        return 4000m;
                    }

                    return null;

                case CarTypes.LocoSteamHeavy:
                case CarTypes.LocoSteamHeavyBlue:
                    if (parameter.Equals("Sand"))
                    {
                        return 1200m;
                    }

                    if (parameter.Equals("Body"))
                    {
                        return 17920m;
                    }

                    if (parameter.Equals("Wheels"))
                    {
                        return 1000m;
                    }

                    return null;

                case CarTypes.LocoShunter:
                    if (parameter.Equals("Fuel"))
                    {
                        return 2000m;
                    }

                    if (parameter.Equals("Oil"))
                    {
                        return 100m;
                    }

                    if (parameter.Equals("Sand"))
                    {
                        return 200m;
                    }

                    if (parameter.Equals("Wheels") || parameter.Equals("Engine"))
                    {
                        return 1000m;
                    }

                    if (parameter.Equals("Body"))
                    {
                        return 5600m;
                    }

                    return null;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}