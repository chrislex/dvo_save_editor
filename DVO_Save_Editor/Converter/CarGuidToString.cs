﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace DVO_Save_Editor.Converter
{
    internal class CarGuidToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || value == null || !(value is string[] tmp))
            {
                return string.Empty;
            }

            var retVal = tmp.Select(guid => Globals.Instance.dataModel.cars.carsData.First(j => j.carGuid.Equals(guid)).id).ToList();
            return string.Join(", ", retVal);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}