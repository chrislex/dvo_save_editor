﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DVO_Save_Editor.Converter
{
    internal class Job_Description_Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || value == null || value.ToString() == string.Empty)
            {
                return string.Empty;
            }

            if (value is LoadJobDefinitionData)
            {
                return "Shunting - Load";
            }

            if (value is UnloadJobDefinitionData)
            {
                return "Shunting - Unload";
            }

            if (value is EmptyHaulJobDefinitionData)
            {
                return "Logistical Haul";
            }

            if (value is TransportJobDefinitionData)
            {
                return "Freight Haul";
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}